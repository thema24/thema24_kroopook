<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><img class="img-circle photo" src="images/test.jpg" ></li>

        <li> <a href="" class="name"><?php echo Auth::user()->name; ?></a><li>
           <form method="POST" action="<?php echo URL::route('search_friends'); ?>" class="navbar-form navbar-left" role="search">
              <?php echo Form::token(); ?>
              <input name="search" type="text" class="form-control" id="searchbar" placeholder="Search">
            
            <button type="submit" class="btn btn-default" id="btnsearch"><i class="fa fa-search"></i></button>
          </form>
        <li><a href=""><i class="fa fa-user-plus"></i></a></li>
        <li><a href="<?php echo URL::route('register'); ?>"><i class="fa fa-users"></i></a></li>
        <li><a href=""><i class="fa fa-star"></i></a></li>

      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>



