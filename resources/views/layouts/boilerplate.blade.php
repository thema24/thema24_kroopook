<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php if(isset($title)){ echo $title; } else{ echo 'CMS'; } ?></title>

           
        <link href="<?php echo Config::get('app.url'); ?>/css/main.css" rel="stylesheet">    
        <link href="<?php echo Config::get('app.url'); ?>/css/logs.css" rel="stylesheet">  

        <?php if(!Auth::check()): ?>
            <link href="<?php echo Config::get('app.url'); ?>/css/home.css" rel="stylesheet">    
        <?php endif; ?>

        <link href="<?php echo Config::get('app.url'); ?>/bower_resources/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo Config::get('app.url'); ?>/bower_resources/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo Config::get('app.url'); ?>/bower_resources/ekko-lightbox/dist/ekko-lightbox.min.css" rel="stylesheet">
        <link href="<?php echo Config::get('app.url'); ?>/bower_resources/bootstrap-chosen/bootstrap-chosen.less" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Poiret+One|Open+Sans+Condensed:300|Raleway|Montserrat|Indie+Flower' rel='stylesheet' type='text/css'>
         <link href="<?php echo Config::get('app.url'); ?>/css/style.css" rel="stylesheet">    


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <?php if(Session::has('error')): ?>
            <div class="alert alert-danger"><?php echo Session::get('error'); ?></div>
        <?php endif; ?>
        <?php if(Session::has('success')): ?>
            <div class="alert alert-success"><?php echo Session::get('success'); ?></div>
        <?php endif; ?>

        <?php if($errors->count() > 0 ): ?>
            <div class="alert alert-danger">
        <?php foreach($errors->all() as $message): ?>
            - <?php echo $message ?> <br />
        <?php endforeach; ?>
            </div>
        <?php endif; ?>
                        @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <?php if(Auth::check()): ?>  
                        @include('layouts.template')
                    <?php endif; ?>

            <div class="content">
            
                @yield('content')

            </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo Config::get('app.url'); ?>/bower_resources/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo Config::get('app.url'); ?>/js/init.js"></script>
        <script src="<?php echo Config::get('app.url'); ?>/bower_resources/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo Config::get('app.url'); ?>/bower_resources/ekko-lightbox/dist/ekko-lightbox.min.js"></script>


    </body>
</html>