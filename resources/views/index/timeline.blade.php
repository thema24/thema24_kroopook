@extends('layouts.boilerplate')
@section('content')

<div class="row">

	<div class="col-md-8 .col-xs-12 .col-sm-8 timeline-container">
			
    <div class="page-header post-area">
        <h2 id="timeline">What is on your mind?</h1>
			 <form class="form-inline">
			 
			  <div class="form-group post-text-area">
			     <textarea class="form-control post-text-area" rows="3"></textarea>
			  </div>
			  <button type="submit" class="btn btn-default">Post</button>
			</form>
       
    </div>
    <ul class="timeline">
        <li>
           <div class="timeline-badge"><img class="img-circle postphoto" src="images/test.jpg" ></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">Mussum ipsum cacilds</h4>
              <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> 11 hours ago via Twitter</small></p>
            </div>
            <div class="timeline-body">
              <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
            </div>
            <div class="buttons">
              <a class="btn postbutton">Like</a>
              23<i class="fa fa-thumbs-up posticon"></i>
              <a href="" class="reactions"> 12 <i class="fa fa-comments posticon"></i></a>
            </div>
          </div>
        </li>


        <li class="timeline-inverted">
          <div class="timeline-badge"><img class="img-circle postphoto" src="images/test.jpg" ></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">Mussum ipsum cacilds</h4>
            </div>
            <div class="timeline-body">
              <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
              <p>Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis. Interagi no mé, cursus quis, vehicula ac nisi. Aenean vel dui dui. Nullam leo erat, aliquet quis tempus a, posuere ut mi. Ut scelerisque neque et turpis posuere pulvinar pellentesque nibh ullamcorper. Pharetra in mattis molestie, volutpat elementum justo. Aenean ut ante turpis. Pellentesque laoreet mé vel lectus scelerisque interdum cursus velit auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ac mauris lectus, non scelerisque augue. Aenean justo massa.</p>
            </div>
          </div>
        </li>
        <li>
           <div class="timeline-badge"><img class="img-circle postphoto" src="images/test.jpg" ></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">Mussum ipsum cacilds</h4>
            </div>
            <div class="timeline-body">
              <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
            </div>
          </div>
        </li>
        
    </ul>

	</div>
	<div class="cold-md-4 col-sm-4 col-xs-12 rightbar-container">
        <div class="rankingboard">
          <table class="table table-striped">
              <caption> Rankingboard </caption>
               <thead>
                <tr>
                  <th> Nr. </th>
                  <th> Group </th>
                  <th> Points </th>
                </tr>
               </thead>
               <tbody>
                <tr>
                  <th>1</th>
                  <th>$cashmoneyGroup</th>
                  <th>250</th>
                </tr>
                <tr>
                  <th>2</th>
                  <th>$cashmoneyGroup</th>
                  <th>250</th>
                </tr>
                <tr>
                  <th>3</th>
                  <th>$cashmoneyGroup</th>
                  <th>250</th>
                </tr>
               </tbody>
          </table>

      </div>
      <div class="btn-group-vertical" role="group" >
      
        <button type="button" class="btn btn-default rightbutton">Groups</button>
        <button type="button" class="btn btn-default rightbutton">Friends</button>
      </div>
    </ul>
	</div>


			
</div>
@stop