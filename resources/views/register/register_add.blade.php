@extends('layouts.boilerplate')

@section('content')


<form role="form" class="users_edit" method="POST">

  <?php echo Form::token(); ?>

  <div class="container">

    <h2><?php echo $title ;?></h2>

    <div class="row">
      <div class="form-group col-xs-12 col-sm-5 col-md-5">
        <?php echo Form::label('name', 'Name', array('class' => 'control-label')); ?>
        <?php echo Form::text('name', Input::old('name'), array('class' => 'form-control')) ?>
      </div>
      <div class="form-group col-xs-12 col-sm-5 col-md-5">
        <?php echo Form::label('gender', 'Gender', array('class' => 'control-label')); ?>
        <div class="radio">
          <label>
            <?php echo Form::radio('gender', 'Men', Input::old('gender') === 'Men' ? 'checked' : ''); ?>
            Men
          </label>
          <label>
            <?php echo Form::radio('gender', 'Women', Input::old('gender') === 'Women' ? 'checked' : ''); ?>
            Women
          </label>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="form-group col-xs-12 col-sm-5 col-md-5">
        <?php echo Form::label('password', 'Password', array('class' => 'control-label')); ?>
        <?php echo Form::password('password', array('class' => 'form-control')); ?>
      </div>
      <div class="col-xs-12 col-sm-5 col-md-5">
        <div class="form-group">
          <?php echo Form::label('password_confirmation', 'Password confirmation', array('class' => 'control-label')); ?>
          <?php echo Form::password('password_confirmation', array('class' => 'form-control')); ?>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="form-group col-xs-12 col-sm-5 col-md-5">
        <?php echo Form::label('email', 'Emailadres', array('class' => 'control-label')); ?>
        <?php echo Form::text('email', Input::old('email'), array('class' => 'form-control')) ?>
      </div>
    </div>


    <button name="submit" type="submit" class="btn btn-primary">Registreren</button>
  </div>
</form>
@stop