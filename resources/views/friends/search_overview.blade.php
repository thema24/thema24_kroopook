@extends('layouts.boilerplate')
@section('content')

<table class="table table-striped">
	<thead>
        <tr>
          <th>Name</th>
          <th>Add as friend</th>
        </tr>
      </thead>
	  <tbody>
		  <?php foreach($users as $user): ?>
		    <tr>
				<td><a href="<?php echo URL::route('user_profile', ['id' => $user->id]); ?>"><?php echo $user->name; ?></a></td>
				<td><a href="<?php echo URL::route('send_friend_request', ['id' => $user->id]); ?>" class="btn btn-primary">Add me as a friend</a></td>
			</tr>
		<?php endforeach; ?>
	  </tbody>
</table>
	
@stop

