<?php

//use Post;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/mongodb', function() {
	$post = DB::connection('mongodb')->collection('posts')->get();
	//dd($post);
	//return $post;
/*	$user = new Post();
	$user->name = 'John';
	$user->save();*/
});

Route::get('/', array('as' => 'homepage', 'uses' => 'Index\IndexController@index'));
Route::post('/', array('uses' => 'Index\IndexController@login'));


Route::get('/register', array('as' => 'register', 'uses' =>'Index\IndexController@register'));
Route::post('/register', 'Index\IndexController@register');


//Route::get('/admin', array('as' => 'admin', 'uses' => 'Admin\AdminLoginController@index'));
//Route::post('/admin', 'Admin\AdminLoginController@login');
//
//Route::get('/timeline','Index\IndexController@timeline');


Route::group(array('middleware' => 'auth', 'prefix' => ''), function()
{
	/* Logout admin route */
	Route::get('/logout', array('as' => 'logout', 'uses' => 'Index\IndexController@logout'));

	/* Logs admin routes */
	Route::group(array('prefix' => 'logs/', 'middleware' => 'roles', 'roles' => ['Administrator']), function()
	{
		Route::get('', array('as' => 'admin_logs', function()
		{
			$today = date('Y-m-d');
			return Redirect::to('/admin/logs/'.$today.'/all');
		}));

		Route::get('{date}/{level}', array('as' => 'admin_logs_level','uses' => 'Admin\Logs\AdminLogsController@log'));
		Route::get('{date}/all',array('as' => 'admin_logs_date_earlier', 'uses' => 'AdminLogsController@log'));
	});

	Route::group(array('prefix' => 'posts/', 'namespace' => 'Posts'), function()
	{
		Route::get('edit', array('as' => 'post_edit', 'uses' => 'PostsController@edit'));
		Route::post('edit', 'PostsController@edit');
	});


	Route::group(array('prefix' => 'groups/', 'namespace' => 'Groups'), function()
	{
		Route::get('edit', array('as' => 'groups_edit', 'uses' => 'GroupsController@edit'));
		Route::post('edit', 'GroupsController@edit');
	});

	Route::group(array('prefix' => 'friends','namespace' => 'Friends'), function()
	{
		Route::get('/sendfriendrequest/{id}', array('as' => 'send_friend_request', 'uses' => 'FriendsController@sendFriendRequest'));
		Route::get('/accept', array('as' => 'friend_accept', 'uses' => 'FriendsController@acceptFriendRequest'));

		//Route::post('/sendfriendrequest', array('as' => 'send_friend_request', 'uses' => 'FriendsController@sendFriendRequest'));

		Route::post('/search', array('as' => 'search_friends', 'uses' => 'FriendsController@search'));
		
		
		
	});
	/* User admin routes */
	Route::group(array('prefix' => 'users/', 'namespace' => 'Admin\Users', 'middleware' => 'roles', 'roles' => ['Administrator','Community Manager', 'Bedrijf']), function()
	{
		Route::get('profile/{id}', array('as' => 'user_profile'));
		Route::get('edit/{id}', array('as' => 'admin_users_edit_id', 'uses' => 'AdminUsersController@edit'));
		Route::post('edit/{id}', array('as' => 'admin_users_edit_id', 'uses' => 'AdminUsersController@edit'));
	});
});