<?php namespace App\Http\Controllers\Groups;

use App\Http\Controllers\BaseController;
use View;
use Auth;
use App\User;
use App\Group;
use Redirect;
use Input;
use Hash;
use Validator;

class GroupsController extends BaseController
{
	public function edit()
	{
		if($_POST)
		{
			// Create a new user object
			$group = new Group();

			// validation rukles
			$rules = array(
				'name'			=>	'required',
			);

			// validator 
        	$validator = Validator::make(Input::all(), $rules);
        	if($validator->fails())
        	{
        		return Redirect::route('groups_edit')->withInput()->withErrors($validator);
        	}
        	else
        	{
        		// assign the form element to the object
        		$group->name			=	Input::get('name');
        		$group->slogan			=	Input::get('slogan');
        		$group->owner_id		=	Auth::user()->id;

				$group->save();

				// redirect to the homepage with message
				return Redirect::route('homepage')->with('success', 'Made a group motherfuckers');
        	}
		}
		return view('groups.groups_edit')
				->with('title', 'Groep aanmaken');
	}

}