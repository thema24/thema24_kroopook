<?php namespace App\Http\Controllers\Index;

use App\Http\Controllers\BaseController;
use View;
use Auth;
use App\User;
use Redirect;
use Input;
use Hash;
use Validator;

class IndexController extends BaseController
{
		//protected $user;

	public function timeline()
	{
		return view('index.timeline');
	}

	public function index()
	{
		return view('index.index_overview');
	}

	public function login()
	{
		if($_POST)
		{
			$rules = array(
				'email'		=>	'required|email',
				'password'	=>	'required'
			);

			$validator = Validator::make(Input::all(), $rules);

			if($validator->fails())
			{
				return Redirect::route('homepage')->withErrors($validator)->withInput(Input::except('wachtwoord'));
			}
			else
			{
				$credentials = array(
					'email'		=>	Input::get('email'),
					'password'	=>	Input::get('password'),
					//'verified'	=>	1,
				);

				if(Auth::attempt($credentials))
				{
					return Redirect::route('homepage')->with('success', 'U bent ingelogd in het CMS');
				}
				else
				{
					return Redirect::route('admin')->with('error', 'De combinatie emailadres & wachtwoord is onjuist')->withInput(Input::except('wachtwoord'));
				}
			}
		}
	}

	public function register()
	{
		if($_POST)
		{
			// Create a new user object
			$user = new User();

			// validation rukles
			$rules = array(
				'name'			=>	'required',
				'gender'		=>	'required',
				'email'			=>	'required|email|unique:users'
			);
			
			if(Input::has('wachtwoord'))
			{
				$rules['password']	=	'required|numeric|min:6||confirmed';
			}

			// validator 
        	$validator = Validator::make(Input::all(), $rules);
        	if($validator->fails())
        	{
        		return Redirect::route('register')->withInput(Input::except('password'))->withErrors($validator);
        	}
        	else
        	{
        		// assign the form element to the object
        		$user->email			=	Input::get('email');
        		$user->name 			=	Input::get('name');
        		$user->gender 			=	Input::get('gender');

        		if(Input::has('password'))
				{
					$user->password	= Hash::make(Input::get('password'));
				}
				$user->save();

				// log the user in when they is registered
				Auth::login($user);

				// redirect to the homepage with message
				return Redirect::route('homepage')->with('success', 'Succesvol geregistreerd!');
        	}
		}

		// View
		return view('register.register_add')
				->with('title', 'Register as a new user');
	}

	public function logout()
	{
		Auth::logout();
		return Redirect::route('homepage')->with('sucesss', 'U bent succesvol uitgelogd');
	}
}