<?php namespace App\Http\Controllers\Posts;

use App\Http\Controllers\BaseController;
use View;
use Auth;
use App\User;
use App\Post;
use Redirect;
use Input;
use Hash;
use Validator;

class PostsController extends BaseController
{
    //protected $connection = 'mongodb';

	public function index()
	{
		return view('index.index_overview');
	}

	public function edit()
	{
		if($_POST)
		{
			// Create a new user object
			$post = new Post();

			// validation rukles
			$rules = array(
				'text'			=>	'required',
			);

			// validator 
        	$validator = Validator::make(Input::all(), $rules);
        	if($validator->fails())
        	{
        		return Redirect::route('post_edit')->withInput(Input::except('password'))->withErrors($validator);
        	}
        	else
        	{
        		// assign the form element to the object
        		$post->text				=	Input::get('text');
        		$post->user_id			=	Auth::user()->id;

				$post->save();

				// redirect to the homepage with message
				return Redirect::route('homepage')->with('success', 'Post placed in mongodb, yeah motherfucker');
        	}
		}

		// View
		return view('posts.posts_add')
				->with('title', 'Post a new message');
	}
}