<?php namespace App\Http\Controllers\Friends;

use App\Http\Controllers\BaseController;
use View;
use Auth;
use App\User;
use App\Group;
use Redirect;
use Input;
use Hash;
use Validator;
use App\Friend;

class FriendsController extends BaseController
{

	public function search()
	{
		if($_POST)
		{
			$users = User::where('name', 'LIKE', '%'.Input::get('search').'%')->get();

		}

		return view('friends.search_overview')
				->with('title', 'Zoekresultaten voor \''.Input::get('search').'\'')
				->with('users', $users);
	}


	public function sendFriendRequest($id)
	{
	/*	if($_POST)
		{*/

		
        		$friend = new Friend();

        		$friend->friend_one	= Auth::user()->id;
        		$friend->friend_two = $id;
        		$friend->status = 1;

				$friend->save();

				// redirect to the homepage with message
				return Redirect::route('homepage')->with('success', 'friend request sended');

		//}
	}

	public function acceptFriendRequest($id = null)
	{
		if($_POST)
		{
			$friend = Friend::find($id);

			$friend->status = 2;
			$friend->save();
			return Redirect::route('homepage')->with('success', 'Hooker i accept this friendship');
		}

		$requests = Friend::where('friend_two', Auth::user()->id)->get();

		dd($requests);
		//dd($requests);
		$users = User::whereIn('id', $requests)->get();

		dd($users);

		return view('friends.friends_accept_overview')
					->with('requests', $requests);
	}

}