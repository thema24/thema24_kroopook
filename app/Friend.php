<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friend extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'friends';


}
